export class CdaPage {

    getCpf() {
      return cy.get('#cpfCnpjInput')
    }
  
    getName() {
      return cy.get('#nomeCompletoInput')
    }
  
    getTel() {
      return cy.get('#telefoneInput')
  
    }
  
    getEmail() {
      return cy.get('#emailInput')
  
    }
}