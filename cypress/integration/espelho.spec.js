describe('#1 Teste Espelho', () => {
    before(() => {
        cy.visit('https://sso-int.hml.digital-segurosunimed.com/Account/Login?ReturnUrl=%2Fconnect%2Fauthorize%2Fcallback%3Fresponse_type%3Dcode%26client_id%3D2f297ce1-348d-4590-b505-aab07a1bb803-usuario-espelho%26state%3DZW12bXQwTks4akdxRHE5OXJpZzNoMWY5b2RBdVVDYlRjd1pHMFozYlljTjBW%26redirect_uri%3Dhttps%253A%252F%252Fdev-usuarioespelho.segurosunimed.tokenlab.dev%26scope%3Dopenid%2520profile%2520usuario-espelho-padrao%2520usuario-espelho-admin%26code_challenge%3DjcY4xbn7XWntFR1a_vYYsGrq-65EpkpfuhJ2stV7w2A%26code_challenge_method%3DS256%26nonce%3DZW12bXQwTks4akdxRHE5OXJpZzNoMWY5b2RBdVVDYlRjd1pHMFozYlljTjBW')
    })

    it('Visitar página e validar campos de input', () => {
        cy.get('#Username').type('maria@teste.com.br')
        cy.get('#Password').type('Teste123*')
    })

    it('Visitar página e validar campos de input', () => {
        cy.get('#Username').type('maria@teste@com.br').should()
        cy.get('#Password').type('Teste123*')
    })

    it('Visitar página e validar campos de input', () => {

        cy.get('#Username')
            .click()
            .should().empty
    })
})

describe.only('#2 Teste Espelho', () => {
    before(() => {
        cy.visit('https://sso-int.hml.digital-segurosunimed.com/Account/Login?ReturnUrl=%2Fconnect%2Fauthorize%2Fcallback%3Fresponse_type%3Dcode%26client_id%3D2f297ce1-348d-4590-b505-aab07a1bb803-usuario-espelho%26state%3DZW12bXQwTks4akdxRHE5OXJpZzNoMWY5b2RBdVVDYlRjd1pHMFozYlljTjBW%26redirect_uri%3Dhttps%253A%252F%252Fdev-usuarioespelho.segurosunimed.tokenlab.dev%26scope%3Dopenid%2520profile%2520usuario-espelho-padrao%2520usuario-espelho-admin%26code_challenge%3DjcY4xbn7XWntFR1a_vYYsGrq-65EpkpfuhJ2stV7w2A%26code_challenge_method%3DS256%26nonce%3DZW12bXQwTks4akdxRHE5OXJpZzNoMWY5b2RBdVVDYlRjd1pHMFozYlljTjBW')
    })
    
    beforeEach(() => {
        cy.reload()
    })

    it('Validar textos da página a partir do body', () => {    
        cy.get('body').should('contain', 'Login')
        cy.get('body').should('contain', 'Remember Login')
        cy.get('body').should('contain', 'CDA')
        cy.get('body').should('contain', 'Register Now!')
        cy.get('body').should('contain', 'Forgot password?')
    })

    it('Validar textos da página a partir do da classe dos elementos esperados', () => {
        cy.get('h1').should('have.text', 'Login')


    })
})
