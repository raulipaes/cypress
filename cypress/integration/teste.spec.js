describe('Teste #1 QA Ninja', () =>{
    before

    it('Validando opção "Radio Buttons"', () => {
    cy.visit('https://training-wheels-protocol.herokuapp.com/');
    cy.get(':nth-child(3) > a').should('contain', 'Radio Buttons')
    });

    it('Validando Título', () => {
        cy.visit('https://training-wheels-protocol.herokuapp.com/');
        cy.title().should('be.equal', 'Training Wheels Protocol')
    })
});
