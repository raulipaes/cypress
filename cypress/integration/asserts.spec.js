/// <reference types="cypress"/>

/*Comparar igualdade de variáveis*/

it('Equality', () => {
    const a = 1;

    expect(a).equal(1);
    expect(a, 'Deveria ser 1').equal(1);
    /* .to.be.equal funciona tanto quanto .equal apenas*/
    expect(a).to.be.equal(1);
    expect(a).not.equal('b');
})

it('Truthy', () => {
    const a = true;
    const b = null;
    let c;

    expect(a).to.be.true;
    expect(true).to.be.true;
    expect(b).to.be.null;
    expect(a).to.be.not.null;
    expect(c).to.be.undefined;
})

it('Object Equality', () => {
    const obj = {
        a: 1,
        b: 2,
    }

    expect(obj).equal(obj)
    expect(obj).equals(obj)
    expect(obj).eq(obj)
    expect(obj).to.be.equal(obj)
    expect(obj).to.be.deep.equal({a:1, b:2})
    expect(obj).eql({a:1, b:2})
    expect(obj).include({a: 1})
    expect(obj).to.have.property('b')
    expect(obj).to.have.property('b', 2) /* Confere se existe a propriedade 'b' e se o valor dela é 2 */
    expect(obj).to.not.be.empty
    expect({}).to.be.empty
})

it('Arrays', () => {
    const vetor = [1,2,3]
    expect(vetor).members([1, 2, 3])
    expect(vetor).to.have.members([1, 2, 3])
    expect(vetor).to.include.members([1, 3]) /* Para conferir se apenas parte do array exsite */
    expect(vetor).to.not.be.empty
    expect([]).to.be.empty
})

it('Types', () => {
    const num = 1
    const str = 'String'

    expect(num).to.be.a('number')
    expect(str).to.be.a('string')
    expect({}).to.be.an('object')
    expect([]).to.be.an('array')
})

it('String', () => {
    let str = 'String de Teste'

    expect(str).to.be.equal('String de Teste')
    expect(str).to.have.length(15)
    expect(str).to.contains('de')
    expect(str).to.match(/de/)
    expect(str).to.match(/^String/) /* o símbolo '^' busca no inicio da sentença */
    expect(str).to.match(/Teste$/) /* o símbolo '$' busca no final da sentença */

})